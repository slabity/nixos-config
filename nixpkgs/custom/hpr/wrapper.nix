{ stdenv, makeWrapper, callPackage
, hprgui ? callPackage ./hprgui.nix {}
, oraclejdk
, wrapperName, binArg
}:

stdenv.mkDerivation rec {
  name = "hpr-${wrapperName}-wrapper-${version}";
  version = "0.1";

  unpackPhase = "true";

  buildInputs = [ makeWrapper ];

  installPhase = ''
    mkdir -p $out/bin

    CLASSPATH=${hprgui}/share/java/*:${hprgui}/share/java/hprRmGUI.jar

    makeWrapper ${oraclejdk}/bin/java $out/bin/${wrapperName} \
      --add-flags "-cp $CLASSPATH com.hpr.rc.RiskConsoleMDIApp ${binArg}"
  '';
}
