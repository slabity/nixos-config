{ stdenv, fetchurl, unzip }:

stdenv.mkDerivation rec {
  name = "hprgui-unwrapped-${version}";
  version = "3.4.97";
  date = "14May2019";

  src = fetchurl {
    url = "http://hpr-share/Builds/gui/riskconsole/release-${version}-${date}/HPR-RiskConsole-${version}.zip";
    sha256 = "0rlj52ml731qjcs7jnmgmhypn59j3xcp378bnh4zwl384jdzwvhv";
  };

  buildInputs = [ unzip ];

  installPhase = ''
    mkdir -p $out/share/java
    mv hprRmGUI.jar $out/share/java
    mv lib/* $out/share/java/
  '';

  meta = with stdenv.lib; {
    description = "a set of java objects for running HPR GUI programs";
    license = licenses.unfree;
    platforms = platforms.linux;
    maintainers = [ "Tyler Slabinski" ];
  };
}
