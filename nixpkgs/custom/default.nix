self: super:
{
  powerlevel10k = super.callPackage ./powerlevel10k.nix {};

  steam = super.steam.override ({
    extraPkgs = p: with p; [
      usbutils
      lsb-release
      procps
      dbus_daemon
      python3
      harfbuzz
      freetype
    ];
  });

  preferredRustChannel = super.rustChannelOf {
    date = "2019-05-25";
    channel = "nightly";
  };

  Fabric = super.python2Packages.Fabric.overridePythonAttrs (oldAttrs: rec {
    pname = "Fabric";
    version = "1.14.1";

    src = super.python2Packages.fetchPypi {
      inherit pname version;
      sha256 = "1a3ndlpdw6bhn8fcw1jgznl117a8pnr84az9rb5fwnrypf1ph2b6";
    };

    checkPhase = "ls -lha";
  });

  hpr = {
    riskconsole = super.callPackage ./hpr/wrapper.nix {
      wrapperName = "riskconsole";
      binArg = "-newriskconsole";
    };

    monitoringconsole = super.callPackage ./hpr/wrapper.nix {
      wrapperName = "monitoringconsole";
      binArg = "-newmonitor";
    };
  };

  adapta-gtk-theme = super.adapta-gtk-theme.overrideDerivation (oldAttrs: rec {
    configureFlags = [
      "--disable-unity"
      "--disable-gtk_legacy"
      "--enable-gtk_next"

      "--with-selection_color=#CC4349"
      "--with-accent_color=#CC6E13"
      "--with-suggestion_color=#CC8743"
      "--with-destruction_color=#CC8743"
    ];
  });
}
