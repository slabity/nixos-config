{ pkgs, ... }:
{
  xsession = {
    enable = true;
    windowManager.i3 = {
      enable = true;
      config = {
        modifier = "Mod4";
        bars = [];
        colors = {
          focused = {
            border = "#FF1D04";
            background = "#FF1D04";
            text = "#FF1D04";
            indicator = "#FF1D04";
            childBorder = "#FF1D04";
          };
          focusedInactive = {
            border = "#F07B6C";
            background = "#F07B6C";
            text = "#F07B6C";
            indicator = "#F07B6C";
            childBorder = "#F07B6C";
          };
          unfocused = {
            border = "#6A3D37";
            background = "#6A3D37";
            text = "#6A3D37";
            indicator = "#6A3D37";
            childBorder = "#6A3D37";
          };
          urgent = {
            border = "#900000";
            background = "#900000";
            text = "#900000";
            indicator = "#900000";
            childBorder = "#900000";
          };
          placeholder = {
            border = "#F07B6C";
            background = "#F07B6C";
            text = "#F07B6C";
            indicator = "#F07B6C";
            childBorder = "#F07B6C";
          };
        };
        floating = {
          modifier = "Mod4";
          titlebar = false;
        };
        gaps = {
          inner = 8;
        };
        keybindings = let modifier = "Mod4"; in {
          "${modifier}+Return" = "exec termite";
          "${modifier}+Shift+Return" = "exec i3-sensible-terminal";
          "${modifier}+Shift+q" = "kill";
          "${modifier}+d" = "exec  --no-startup-id rofi -show run -font 'Terminess Powerline 9'";
          "${modifier}+k" = "focus up";
          "${modifier}+j" = "focus down";
          "${modifier}+h" = "focus left";
          "${modifier}+l" = "focus right";
          "${modifier}+a" = "focus parent";
          "${modifier}+space" = "focus mode_toggle";
          "${modifier}+Shift+k" = "move up";
          "${modifier}+Shift+j" = "move down";
          "${modifier}+Shift+h" = "move left";
          "${modifier}+Shift+l" = "move right";
          "${modifier}+v" = "split v";
          "${modifier}+s" = "layout stacking";
          "${modifier}+w" = "layout tabbed";
          "${modifier}+e" = "layout toggle split";
          "${modifier}+Shift+space" = "floating toggle";
          "${modifier}+f" = "fullscreen toggle";
          "${modifier}+1" = "workspace 1";
          "${modifier}+2" = "workspace 2";
          "${modifier}+3" = "workspace 3";
          "${modifier}+4" = "workspace 4";
          "${modifier}+5" = "workspace 5";
          "${modifier}+6" = "workspace 6";
          "${modifier}+7" = "workspace 7";
          "${modifier}+8" = "workspace 8";
          "${modifier}+9" = "workspace 9";
          "${modifier}+0" = "workspace 10";
          "${modifier}+Shift+1" = "move container to workspace 1";
          "${modifier}+Shift+2" = "move container to workspace 2";
          "${modifier}+Shift+3" = "move container to workspace 3";
          "${modifier}+Shift+4" = "move container to workspace 4";
          "${modifier}+Shift+5" = "move container to workspace 5";
          "${modifier}+Shift+6" = "move container to workspace 6";
          "${modifier}+Shift+7" = "move container to workspace 7";
          "${modifier}+Shift+8" = "move container to workspace 8";
          "${modifier}+Shift+9" = "move container to workspace 9";
          "${modifier}+Shift+0" = "move container to workspace 10";
          "${modifier}+Shift+c" = "reload";
          "${modifier}+Shift+r" = "restart";
        };
      };
    };
  };

  programs.termite = {
    enable = true;
    font = "Monospace 9";
    backgroundColor = "#1a1414";
    foregroundColor = "#f2c2c2";
    foregroundBoldColor = "#f2c2c2";
    cursorColor = "#f2c2c2";
    colorsExtra = ''
        # black
        color0  = #4d0000
        color8  = #cc0000

        # red
        color1  = #8c1515
        color9  = #ff3333

        # green
        color2  = #a62121
        color10 = #ff4d4d

        # yellow
        color3  = #b33e3e
        color11 = #ff6666

        # blue
        color4  = #bf6060
        color12 = #ff8080

        # magenta
        color5  = #997373
        color13 = #e69595

        # cyan
        color6  = #998e8e
        color14 = #ffcccc

        # white
        color7  = #665252
        color15 = #231919
    '';
  };

  services.compton = {
    enable = true;
    backend = "xrender";
    vSync = "drm";
    activeOpacity = "1";
    inactiveOpacity = "0.95";
    menuOpacity = "1";
    blur = true;
    shadow = true;
    fade = true;
    fadeDelta = 3;
    opacityRule = [ "100:_NET_WM_NAME@:s *?= 'rofi'"];
  };

  services.redshift = {
    enable = true;
    latitude = "43";
    longitude = "-71";
    brightness.day = "1";
    brightness.night = "0.5";
    temperature.day = 6500;
    temperature.night = 3500;
  };

  gtk = {
    enable = true;
    font = {
      name = "Noto Sans 8";
    };
    iconTheme = {
      package = pkgs.arc-icon-theme;
      name = "Arc";
    };
    theme = {
      package = pkgs.adapta-gtk-theme;
      name = "Adapta-Nokto-Eta";
    };
  };

  services.dunst = {
    enable = true;
    settings = {
      global = {
        font = "Mono 9";
        allow_markup = true;

        format = "<b>%s</b>\n%b";

        sort = true;
        indicate_hidden = true;
        bounce_freq = 0;
        show_age_threshold = 60;
        word_wrap = true;
        ignore_newline = false;

        geometry = "600x5-20+45";
        shrink = false;

        transparency = 0;
        idle_threshold = 0;
        monitor = 0;
        follow = "mouse";
        sticky_history = true;
        history_length = 20;
        show_indicators = true;

        line_height = 0;
        separator_height = 2;
        padding = 9;
        horizontal_padding = 9;
        separator_color = "frame";

        startup_notification = false;
      };
      frame = {
        width = 3;
        color = "#ebdbb2";
      };
      shortcuts = {
        close = "ctrl+space";
        close_all = "ctrl+shift+space";
        history = "ctrl+grave";
        context = "ctrl+shift+period";
      };
      urgency_low = {
        background = "#282828";
        foreground = "#ebdbb2";
        timeout = 5;
      };
      urgency_normal = {
        background = "#282828";
        foreground = "#ebdbb2";
        timeout = 5;
      };
      urgency_critical = {
        background = "#cc241d";
        foreground = "#ebdbb2";
        timeout = 0;
      };
    };
  };

  xresources.extraConfig = builtins.readFile "${./x11/Xresources}";
}
