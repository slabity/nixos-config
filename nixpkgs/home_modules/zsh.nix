{ pkgs, ... }:
{
  programs.zsh = {
    enable = true;
    enableAutosuggestions = true;
    enableCompletion = true;
    dotDir = ".config/zsh";
    history = {
      expireDuplicatesFirst = true;
      extended = true;
      ignoreDups = true;
      path = ".config/zsh/history";
      save = 100000;
      share = true;
      size = 100000;
    };
    oh-my-zsh = {
      enable = true;
      plugins = [
        "common-aliases"
        "compleat"
        "dirhistory"
        "encode64"
        "fasd"
        "git"
        "git-extras"
        "git-prompt"
        "nix"
        "per-directory-history"
        "sudo"
        "systemd"
        "vi-mode"
        "wd"
      ];
      theme = "../../../../../..${pkgs.powerlevel10k}/powerlevel10k/powerlevel10k";
    };
    sessionVariables = {
      TERM = "xterm-256color";
    };
    initExtra = ''
      ${builtins.readFile ./powerlevel10k_init.zsh};
      compinit
    '';
  };

  programs.tmux = {
    enable = true;
    extraConfig = ''
      bind-key a set-window-option synchronize-panes\; display-message "synchronize-panes is now #{?pane_synchronized,on,off}"
    '';
    tmuxinator.enable = true;
  };
}
