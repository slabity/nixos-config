{ pkgs, ... }:
{
  programs.home-manager.enable = true;
  programs.home-manager.path = https://github.com/rycee/home-manager/archive/master.tar.gz;

  imports = [
    ./home_modules/desktop.nix
    ./home_modules/neovim.nix
    ./home_modules/zsh.nix
    ];
  programs.git = {
    enable = true;
    userName = "Tyler Slabinski";
    userEmail = "tslabinski@hyannisportresearch.com";
    ignores = [ ".*" ];
  };

  services.kdeconnect.enable = true;

  home.packages = [
    pkgs.zsh-completions
    pkgs.nix-zsh-completions
  ];

}
