{ pkgs, ... }:
{
  imports = [
    ./lib
    ./system.nix
    ./hardware.nix
    ./passwd.nix
    ./nix.nix
    ./profiles
    ./desktop
  ];

  environment.systemPackages = with pkgs; [
    nix-index
    nix-prefetch-git

    unzip zip unrar

    pciutils usbutils atop
    pstree

    file bc psmisc

    git manpages
  ];
}
