{ config, lib, pkgs, ... }:
with lib;
let
  cfg = config.foxos;
  desktopCfg = cfg.desktop;
in
{
  options.foxos.desktop.enable = mkEnableOption "Enable desktop support";

  config = mkIf desktopCfg.enable {
    services.xserver = {
      enable = true;

      layout = "us";
      libinput = {
        enable = true;
        accelProfile = "flat";
        tapping = false;
        tappingDragLock = false;
      };

      displayManager.job.logToJournal = true;
      displayManager.lightdm.enable = true;
      displayManager.lightdm.autoLogin = {
        enable = true;
        user = cfg.mainUser.name;
      };
      displayManager.lightdm.greeter.enable = false;

      desktopManager.xterm.enable = false;

      videoDrivers = [ "amdgpu" "modesetting" ];
      useGlamor = true;

      desktopManager.session = [
        {
          name = "custom";
          start = "";
        }
      ];

      desktopManager.default = "custom";
    };
  };
}
