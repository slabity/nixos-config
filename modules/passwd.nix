{ config, lib, foxlib, pkgs, ... }:
with lib;
let
  cfg = config.foxos;
  userCfg = cfg.mainUser;

  homeMgrPkg = fetchTarball "https://github.com/rycee/home-manager/archive/master.tar.gz";
in
{
  imports = [
    "${homeMgrPkg}/nixos"
  ];

  options.foxos.mainUser = {
    enable = mkEnableOption "Main (non-root) user";
    name = mkOption {
      type = types.nullOr types.str;
      default = null;
      description = "Main user name";
    };
  };

  config = mkIf userCfg.enable {
    users = {
      defaultUserShell = pkgs.zsh;
      mutableUsers = false;

      extraUsers.${userCfg.name} = {
        uid = 1000;
        isNormalUser = true;

        extraGroups = [
          "wheel"
          "networkmanager"
          "docker"
          "libvirtd"
          "dialout"
        ];

        passwordFile = "${../passwd}";
      };
    };

    assertions = [
      (foxlib.reqVar "foxos.mainUser.name" userCfg.name)
    ];
  };
}
