{ ... }:
{
  imports = [
    ./workstation.nix
  ];

  nixpkgs = import ../../nixpkgs;
}
