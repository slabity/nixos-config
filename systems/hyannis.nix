{ config, ... }:
{
  imports = [
    ../modules
    ../hardware-configuration.nix
  ];

  foxos.system.name = "tslabinski-nixos";

  foxos.hardware.efi = true;

  foxos.hardware.cpu = {
    type = "intel";
    sockets = 1;
    cores = 4;
    threads = 2;
    support32Bit = true;
  };

  foxos.hardware.gpu.enable = true;
  foxos.hardware.gpu.type.intel = true;
  foxos.hardware.gpu.type.nvidia = true;

  foxos.hardware.ethernet.enable = true;
  foxos.hardware.wifi.enable = true;
  foxos.hardware.bluetooth.enable = true;
  foxos.hardware.audio.enable = true;
  foxos.hardware.input.enable = true;
  foxos.hardware.battery.enable = true;

  foxos.profiles.workstation.enable = true;

  foxos.mainUser.enable = true;
  foxos.mainUser.name = "tslabinski";

  time.timeZone = "America/New_York";

  services.udev.extraRules = ''
    # Enable BFQ IO Scheduling algorithm
    ACTION=="add|change", KERNEL=="[sv]d[a-z]", ATTR{queue/rotational}=="1", ATTR{queue/scheduler}="bfq"
    ACTION=="add|change", KERNEL=="[sv]d[a-z]", ATTR{queue/rotational}=="0", ATTR{queue/scheduler}="bfq"

    # Ultimate Hacking Keyboard rules
    # These are the udev rules for accessing the USB interfaces of the UHK as non-root users.
    # Copy this file to /etc/udev/rules.d and physically reconnect the UHK afterwards.
    SUBSYSTEM=="input", GROUP="input", MODE="0666"
    SUBSYSTEMS=="usb", ATTRS{idVendor}=="1d50", ATTRS{idProduct}=="612[0-7]", MODE:="0666", GROUP="plugdev"
    KERNEL=="hidraw*", ATTRS{idVendor}=="1d50", ATTRS{idProduct}=="612[0-7]", MODE="0666", GROUP="plugdev"
  '';

  virtualisation.docker.enable = true;
}
